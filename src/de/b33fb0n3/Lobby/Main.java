package de.b33fb0n3.Lobby;

import de.b33fb0n3.Commands.*;
import de.b33fb0n3.Listener.*;
import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Plugin made by B33fb0n3YT
 * 26.07.2020
 * F*CKING SKIDDER!
 * Licensed by B33fb0n3YT
 * © All rights reserved
 */

public class Main extends JavaPlugin {
    private static Main plugin;
    public ConsoleCommandSender console = getServer().getConsoleSender();
    public static String Prefix = "§eSystem §7| §f";
    public static String noPerm = Main.Prefix + "§cDafür hast du keine Rechte!";
    public static List<Integer> slots = Arrays.asList(10, 12, 14, 16);
    public static File settingsFile = new File("plugins/LobbySystem", "settings.yml");
    public static FileConfiguration settings = YamlConfiguration.loadConfiguration(settingsFile);

    @Override
    public void onEnable() {
        plugin = this;

        console.sendMessage(Prefix + "§e[]=======================[]");
        console.sendMessage(Prefix + "						 ");
        console.sendMessage(Prefix + "§2Coded by: §dB33fb0n3YT");
        loadConfig();
        console.sendMessage(Prefix + "§aLobby wurde aktiviert!");
        console.sendMessage(Prefix + "						 ");
        console.sendMessage(Prefix + "§e[]=======================[]");
        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "gamerule doMobSpawn false");
        init();
    }

    private void init() {
        initCommands();
        initListener();
    }

    private void initListener() {
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new BlockPlaceBreak(), this);
        pm.registerEvents(new EntityDamage(), this);
        pm.registerEvents(new Join(), this);
        pm.registerEvents(new InventoryClick(), this);
        pm.registerEvents(new InteractItem(), this);
        pm.registerEvents(new AntiFeed(), this);
        pm.registerEvents(new Chat(), this);
    }

    private void initCommands() {
        getCommand("build").setExecutor(new Build());
        getCommand("setspawn").setExecutor(new SetSpawn());
        getCommand("warp").setExecutor(new Warp());
        getCommand("setwarp").setExecutor(new Setwarp());
        getCommand("spawn").setExecutor(new Spawn());
        getCommand("author").setExecutor(new Author());
    }

    @Override
    public void onDisable() {
        console.sendMessage(Prefix + "§e[]=======================[]");
        console.sendMessage(Prefix + "						 ");
        console.sendMessage(Prefix + "§2Coded by: §dB33fb0n3YT");
        console.sendMessage(Prefix + "§cLobby wurde deaktiviert!");
        console.sendMessage(Prefix + "						 ");
        console.sendMessage(Prefix + "§e[]=======================[]");
    }

    public static Main getPlugin() {
        return plugin;
    }

    public static void error(Exception e) {
        Main.getPlugin().console.sendMessage(Main.Prefix + "§cFEHLER: §b" + e.getLocalizedMessage());
        for(int i = 0; i < e.getStackTrace().length; i++) {
            if(e.getStackTrace()[i].toString().contains("de.b33fb0n3")) {
                Main.getPlugin().console.sendMessage("§b"+ e.getStackTrace()[i]);
            }
        }
    }

    private void loadConfig() {
        settings.options().header("Hier kannst du alles ändern.");
        settings.addDefault("Header", "Header");
        settings.addDefault("Footer", "Footer");
        settings.addDefault("Compassitems.Item1.Name", "Item1");
        settings.addDefault("Compassitems.Item1.ItemID", 1);
        settings.addDefault("Compassitems.Item2.Name", "Item2");
        settings.addDefault("Compassitems.Item2.ItemID", 2);
        settings.addDefault("Compassitems.Item3.Name", "Item3");
        settings.addDefault("Compassitems.Item3.ItemID", 3);
        settings.addDefault("Compassitems.Item4.Name", "Item4");
        settings.addDefault("Compassitems.Item4.ItemID", 4);
        settings.options().copyDefaults(true);
        try {
            settings.save(settingsFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
