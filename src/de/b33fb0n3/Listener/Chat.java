package de.b33fb0n3.Listener;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * Plugin made by B33fb0n3YT
 * 26.07.2020
 * F*CKING SKIDDER!
 * Licensed by B33fb0n3YT
 * © All rights reserved
 */

public class Chat implements Listener {

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        for (Player current : Bukkit.getOnlinePlayers()) {
            if (!current.hasMetadata("chatmute")) {
                e.setCancelled(true);
                current.sendMessage(e.getPlayer().getDisplayName() + " §8» §f" + e.getMessage());
            }
        }
    }

}
