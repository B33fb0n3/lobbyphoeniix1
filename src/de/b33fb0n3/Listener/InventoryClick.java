package de.b33fb0n3.Listener;

import de.b33fb0n3.Lobby.Main;
import de.b33fb0n3.Utils.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

/**
 * Plugin made by B33fb0n3YT
 * 26.07.2020
 * F*CKING SKIDDER!
 * Licensed by B33fb0n3YT
 * © All rights reserved
 */

public class InventoryClick implements Listener {

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        if (p.hasMetadata("builder")) {
            e.setCancelled(false);
            return;
        }
        try {
            if (e.getClickedInventory() == p.getInventory() && !e.getCurrentItem().getType().equals(Material.FEATHER) && !e.getCurrentItem().getType().equals(Material.NAME_TAG)) {
                e.setCancelled(true);
                return;
            }
        } catch (Exception e1) {
            p.closeInventory();
        }
        e.setCancelled(true);
        try {
            if (e.getInventory().getTitle().equalsIgnoreCase("§bSpielauswahl")) {
                p.chat("/warp " + e.getRawSlot());
            }
            ItemStack red = new ItemBuilder(new ItemStack(Material.INK_SACK, 1)).setDurability((short) 1).setName("§cAus").toItemStack();
            ItemStack green = new ItemBuilder(new ItemStack(Material.INK_SACK, 1)).setDurability((short) 10).setName("§aAn").toItemStack();
            if (e.getInventory().getTitle().equalsIgnoreCase("§cEinstellungen")) {
                switch (e.getRawSlot()) {
                    case 20:
                        if (p.hasMetadata("allhidden")) {
                            for (Player current : Bukkit.getOnlinePlayers()) {
                                p.showPlayer(current);
                            }
                            p.removeMetadata("allhidden", Main.getPlugin());
                            p.getOpenInventory().setItem(e.getRawSlot(), red);
                            p.sendMessage(Main.Prefix + "Es werden nun alle Spieler angezeigt");
                        } else {
                            for (Player current : Bukkit.getOnlinePlayers()) {
                                p.hidePlayer(current);
                            }
                            p.setMetadata("allhidden", new FixedMetadataValue(Main.getPlugin(), true));
                            p.getOpenInventory().setItem(e.getRawSlot(), green);
                            p.sendMessage(Main.Prefix + "Es werden nun alle Spieler versteckt");
                        }
                        break;
                    case 22:
                        if (p.hasMetadata("spawnatspawn")) {
                            p.removeMetadata("spawnatspawn", Main.getPlugin());
                            p.sendMessage(Main.Prefix + "Du Spawnst nun nicht mehr beim Spawn");
                            p.getOpenInventory().setItem(e.getRawSlot(), red);
                        } else {
                            p.getOpenInventory().setItem(e.getRawSlot(), green);
                            p.setMetadata("spawnatspawn", new FixedMetadataValue(Main.getPlugin(), true));
                            p.sendMessage(Main.Prefix + "Du Spawnst nun beim Spawn");
                        }
                        break;
                    case 24:
                        if (p.hasMetadata("chatmute")) {
                            p.removeMetadata("chatmute", Main.getPlugin());
                            p.getOpenInventory().setItem(e.getRawSlot(), red);
                            p.sendMessage(Main.Prefix + "Dein Chat wird nun wieder angezeigt");
                        } else {
                            p.getOpenInventory().setItem(e.getRawSlot(), green);
                            p.setMetadata("chatmute", new FixedMetadataValue(Main.getPlugin(), true));
                            p.sendMessage(Main.Prefix + "Dein Chat wird nun versteckt");
                        }
                        break;
                }
            }
            switch (e.getCurrentItem().getItemMeta().getDisplayName()) {
                case "§eFreunde":
                    p.openInventory(InteractItem.profil);
                    break;
                case "§eFreundschaftsanfragen":
                    p.openInventory(InteractItem.fas);
                    p.chat("/friend list");
                    break;
                case "§eParty":
                    p.openInventory(InteractItem.party);
                    p.chat("/party create");
                    p.chat("/party invite");
                    break;
                case "§eErfahrung":
                    p.sendMessage(Main.Prefix + "Aktuell hast du " + p.getLevel() + " Level!");
                    p.closeInventory();
                    break;
                case "§eFreund Hinzufügen":
                    p.chat("/friend add");
                    p.closeInventory();
                    break;
                case "§eFreunde anzeigen":
                    p.chat("/friend list");
                    p.closeInventory();
                    break;
                case "§eFreund entfernen":
                    p.chat("/friend remove");
                    p.closeInventory();
                    break;
                case "§eParty verlassen":
                    p.chat("/party leave");
                    p.closeInventory();
                    break;
                default:
                    break;
            }
        } catch (Exception e1) {
        }
    }
}
