package de.b33fb0n3.Listener;

import de.b33fb0n3.Lobby.Main;
import de.b33fb0n3.Utils.ItemBuilder;
import de.b33fb0n3.Utils.Scoreboard;
import net.minecraft.server.v1_8_R3.ChatMessage;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerListHeaderFooter;
import org.bukkit.*;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Plugin made by B33fb0n3YT
 * 12.07.2020
 * F*CKING SKIDDER!
 * Licensed by B33fb0n3YT
 * © All rights reserved
 */

public class Join implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        p.getInventory().clear();
        p.getInventory().setItem(0, new ItemBuilder(new ItemStack(Material.COMPASS)).setName("§bSpielauswahl §7(Rechtsklick)").toItemStack());
        p.getInventory().setItem(7, new ItemBuilder(new ItemStack(Material.SKULL_ITEM, 1, (short) 3)).setName("§eDein Profil §7(Rechtsklick)").setSkullOwner(e.getPlayer().getName()).toItemStack());
        p.getInventory().setItem(8, new ItemBuilder(new ItemStack(Material.REDSTONE_TORCH_ON)).setName("§cEinstellungen §7(Rechtsklick)").toItemStack());
        p.setGameMode(GameMode.ADVENTURE);
        setTab(p, ChatColor.translateAlternateColorCodes('&', Main.settings.getString("Header")), ChatColor.translateAlternateColorCodes('&', Main.settings.getString("Footer")));
        if(p.hasMetadata("spawnatspawn")) {
            p.chat("/spawn");
        }
        for(Player current : Bukkit.getOnlinePlayers()) {
            if(e.getPlayer().hasMetadata("allhidden")) {
                e.getPlayer().hidePlayer(current);
            }
            Scoreboard.setScoreboard(current);
        }
        try {
            World w = p.getWorld();
            w.getEntities();
            List<Entity> list = w.getEntities();
            for (Entity entity : list) {
                if (!(entity instanceof Player)) {
                    entity.remove();
                }
            }
        } catch (Exception ignored) {
        }
    }

    private void setTab(Player p, String header, String footer) {
        IChatBaseComponent headerB = new ChatMessage(header);
        IChatBaseComponent footerB = new ChatMessage(footer);
        PacketPlayOutPlayerListHeaderFooter tablist = new PacketPlayOutPlayerListHeaderFooter();
        try {
            Field headerField = tablist.getClass().getDeclaredField("a");
            headerField.setAccessible(true);
            headerField.set(tablist, headerB);
            headerField.setAccessible(!headerField.isAccessible());
            Field footerField = tablist.getClass().getDeclaredField("b");
            footerField.setAccessible(true);
            footerField.set(tablist, footerB);
            footerField.setAccessible(!footerField.isAccessible());
        } catch (Exception e) {
            e.printStackTrace();
        }
        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(tablist);
    }

}
