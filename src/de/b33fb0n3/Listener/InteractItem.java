package de.b33fb0n3.Listener;

import de.b33fb0n3.Lobby.Main;
import de.b33fb0n3.Utils.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * Plugin made by B33fb0n3YT
 * 26.07.2020
 * F*CKING SKIDDER!
 * Licensed by B33fb0n3YT
 * © All rights reserved
 */

public class InteractItem implements Listener {

    static Inventory compass;
    static Inventory profil;
    static Inventory fas;
    static Inventory party;
    static Inventory settings;

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        Player p = e.getPlayer();

        if (e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK) || e.getAction().equals(Action.LEFT_CLICK_AIR) || e.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
            try {
                e.setCancelled(true);
                ItemStack fuellung = new ItemBuilder(new ItemStack(Material.STAINED_GLASS_PANE, 1)).setName("§r").setDurability((short) 7).toItemStack();
                switch (e.getItem().getItemMeta().getDisplayName()) {
                    case "§bSpielauswahl §7(Rechtsklick)":
                        p.playSound(p.getLocation(), Sound.CHICKEN_EGG_POP, 15F, 15F);
                        compass = Bukkit.createInventory(null, 3 * 9, "§bSpielauswahl");

                        for (int i = 0; i < 3 * 9; i++) {
                            if (i < 9)
                                compass.setItem(i, fuellung);
                            if (i >= 18)
                                compass.setItem(i, fuellung);
                        }
                        compass.setItem(10, new ItemBuilder(new ItemStack(Material.getMaterial(Main.settings.getInt("Compassitems.Item1.ItemID")), 1)).setName(ChatColor.translateAlternateColorCodes('&', Main.settings.getString("Compassitems.Item1.Name"))).toItemStack());
                        compass.setItem(12, new ItemBuilder(new ItemStack(Material.getMaterial(Main.settings.getInt("Compassitems.Item2.ItemID")), 1)).setName(ChatColor.translateAlternateColorCodes('&', Main.settings.getString("Compassitems.Item2.Name"))).toItemStack());
                        compass.setItem(14, new ItemBuilder(new ItemStack(Material.getMaterial(Main.settings.getInt("Compassitems.Item3.ItemID")), 1)).setName(ChatColor.translateAlternateColorCodes('&', Main.settings.getString("Compassitems.Item3.Name"))).toItemStack());
                        compass.setItem(16, new ItemBuilder(new ItemStack(Material.getMaterial(Main.settings.getInt("Compassitems.Item4.ItemID")), 1)).setName(ChatColor.translateAlternateColorCodes('&', Main.settings.getString("Compassitems.Item4.Name"))).toItemStack());

                        p.openInventory(compass);
                        break;
                    case "§eDein Profil §7(Rechtsklick)":
                        p.playSound(p.getLocation(), Sound.CHICKEN_EGG_POP, 15F, 15F);
                        profil = Bukkit.createInventory(null, 5 * 9, "§eDein Profil");

                        Integer[] fuellungArray = new Integer[]{1, 10, 19, 28, 37, 38, 39, 40, 41, 42, 43, 44};
                        fas = Bukkit.createInventory(null, 5 * 9, "§eDein Profil");
                        party = Bukkit.createInventory(null, 5 * 9, "§eDein Profil");
                        for (int i = 0; i < fuellungArray.length; i++) {
                            profil.setItem(fuellungArray[i], fuellung);
                            fas.setItem(fuellungArray[i], fuellung);
                            party.setItem(fuellungArray[i], fuellung);
                        }
                        profil.setItem(0, new ItemBuilder(new ItemStack(Material.NAME_TAG, 1)).setName("§eFreunde").toItemStack());
                        profil.setItem(1, new ItemBuilder(new ItemStack(Material.STAINED_GLASS_PANE, 1)).setName("§r").setDurability((short) 5).toItemStack());
                        profil.setItem(9, new ItemBuilder(new ItemStack(Material.BOOKSHELF, 1)).setName("§eFreundschaftsanfragen").toItemStack());
                        profil.setItem(18, new ItemBuilder(new ItemStack(Material.CAKE, 1)).setName("§eParty").toItemStack());
                        profil.setItem(21, new ItemBuilder(new ItemStack(Material.INK_SACK, 1)).setDurability((short) 10).setName("§eFreund Hinzufügen").toItemStack());
                        profil.setItem(23, new ItemBuilder(new ItemStack(Material.BOOK, 1)).setName("§eFreunde anzeigen").toItemStack());
                        profil.setItem(25, new ItemBuilder(new ItemStack(Material.INK_SACK, 1)).setDurability((short) 1).setName("§eFreund entfernen").toItemStack());
                        profil.setItem(27, new ItemBuilder(new ItemStack(Material.EXP_BOTTLE, 1)).setName("§eErfahrung").toItemStack());

                        p.openInventory(profil);

                        preFill();
                        break;
                    case "§cEinstellungen §7(Rechtsklick)":
                        p.playSound(p.getLocation(), Sound.CHICKEN_EGG_POP, 15F, 15F);
                        settings = Bukkit.createInventory(null, 4 * 9, "§cEinstellungen");

                        settings.setItem(11, new ItemBuilder(new ItemStack(Material.EYE_OF_ENDER, 1)).setName("§ePlayerhider").toItemStack());
                        if (p.hasMetadata("allhidden"))
                            settings.setItem(20, new ItemBuilder(new ItemStack(Material.INK_SACK, 1)).setDurability((short) 10).setName("§aAn").toItemStack());
                        else
                            settings.setItem(20, new ItemBuilder(new ItemStack(Material.INK_SACK, 1)).setDurability((short) 1).setName("§cAus").toItemStack());

                        settings.setItem(13, new ItemBuilder(new ItemStack(Material.BED, 1)).setName("§eSpawnAtSpawn").toItemStack());
                        if (p.hasMetadata("spawnatspawn"))
                            settings.setItem(22, new ItemBuilder(new ItemStack(Material.INK_SACK, 1)).setDurability((short) 10).setName("§aAn").toItemStack());
                        else
                            settings.setItem(22, new ItemBuilder(new ItemStack(Material.INK_SACK, 1)).setDurability((short) 1).setName("§cAus").toItemStack());

                        settings.setItem(15, new ItemBuilder(new ItemStack(Material.FEATHER, 1)).setName("§eChatmute").toItemStack());
                        if (p.hasMetadata("chatmute"))
                            settings.setItem(24, new ItemBuilder(new ItemStack(Material.INK_SACK, 1)).setDurability((short) 10).setName("§aAn").toItemStack());
                        else
                            settings.setItem(24, new ItemBuilder(new ItemStack(Material.INK_SACK, 1)).setDurability((short) 1).setName("§cAus").toItemStack());

                        p.openInventory(settings);
                        break;
                    default:
                        e.setCancelled(false);
                        break;
                }
            } catch (Exception e2) {
                e.setCancelled(false);
            }
        }
    }

    private void preFill() {
        fas.setItem(0, new ItemBuilder(new ItemStack(Material.NAME_TAG, 1)).setName("§eFreunde").toItemStack());
        fas.setItem(9, new ItemBuilder(new ItemStack(Material.BOOKSHELF, 1)).setName("§eFreundschaftsanfragen").toItemStack());
        fas.setItem(10, new ItemBuilder(new ItemStack(Material.STAINED_GLASS_PANE, 1)).setName("§r").setDurability((short) 5).toItemStack());
        fas.setItem(18, new ItemBuilder(new ItemStack(Material.CAKE, 1)).setName("§eParty").toItemStack());
        fas.setItem(27, new ItemBuilder(new ItemStack(Material.EXP_BOTTLE, 1)).setName("§eErfahrung").toItemStack());

        party.setItem(0, new ItemBuilder(new ItemStack(Material.NAME_TAG, 1)).setName("§eFreunde").toItemStack());
        party.setItem(9, new ItemBuilder(new ItemStack(Material.BOOKSHELF, 1)).setName("§eFreundschaftsanfragen").toItemStack());
        party.setItem(18, new ItemBuilder(new ItemStack(Material.CAKE, 1)).setName("§eParty").toItemStack());
        party.setItem(19, new ItemBuilder(new ItemStack(Material.STAINED_GLASS_PANE, 1)).setName("§r").setDurability((short) 5).toItemStack());
        party.setItem(25, new ItemBuilder(new ItemStack(Material.INK_SACK, 1)).setDurability((short) 1).setName("§eParty verlassen").toItemStack());
        party.setItem(27, new ItemBuilder(new ItemStack(Material.EXP_BOTTLE, 1)).setName("§eErfahrung").toItemStack());
    }
}
