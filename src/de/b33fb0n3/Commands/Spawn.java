package de.b33fb0n3.Commands;

import de.b33fb0n3.Lobby.Main;
import de.b33fb0n3.Utils.ConfigLocation;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Plugin made by B33fb0n3YT
 * 12.07.2020
 * F*CKING SKIDDER!
 * Licensed by B33fb0n3YT
 * © All rights reserved
 */

public class Spawn implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;
            if (args.length == 0) {
                Location toTeleport = new ConfigLocation(Main.getPlugin(), "Warps.Warp.1").loadLocation();
                if (toTeleport == null) {
                    p.sendMessage(Main.Prefix + "§cDieser Warp wurde noch nicht gesetzt!");
                    p.sendMessage(Main.Prefix + "Verwende §e/setspawn");
                    return false;
                }
                p.teleport(toTeleport);
            } else
                p.sendMessage(Main.Prefix + "Verwende §e/spawn");
        }
        return false;
    }
}
