package de.b33fb0n3.Commands;

import de.b33fb0n3.Lobby.Main;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Plugin made by B33fb0n3YT
 * 12.07.2020
 * F*CKING SKIDDER!
 * Licensed by B33fb0n3YT
 * © All rights reserved
 */

public class SetSpawn implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;
            if (p.hasPermission("system.setspawn")) {
                if (args.length == 0) {
                    p.chat("/setwarp 1");
                } else
                    p.sendMessage(Main.Prefix + "Verwende §e/setspawn");
            } else
                p.sendMessage(Main.noPerm);
        }
        return false;
    }
}
