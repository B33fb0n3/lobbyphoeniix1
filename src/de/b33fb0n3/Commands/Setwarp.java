package de.b33fb0n3.Commands;

import de.b33fb0n3.Lobby.Main;
import de.b33fb0n3.Utils.ConfigLocation;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Plugin made by B33fb0n3YT
 * 12.07.2020
 * F*CKING SKIDDER!
 * Licensed by B33fb0n3YT
 * © All rights reserved
 */

public class Setwarp implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;
            if (p.hasPermission("system.setwarp")) {
                if (args.length == 1) {
                    try {
                        if (Main.slots.contains(Integer.parseInt(args[0])) || args[0].equalsIgnoreCase("1")) {
                            new ConfigLocation(Main.getPlugin(), p.getLocation(), "Warps.Warp." + args[0]).saveLocation();
                            p.sendMessage(Main.Prefix + "Der Warp wurde gesetzt!");
                        } else
                            p.sendMessage(Main.Prefix + "§cVerwende die richtigen Zahlen!");
                    } catch (NumberFormatException e) {
                        p.sendMessage(Main.Prefix + "§cVerwende eine Zahl!");
                    }
                } else
                    p.sendMessage(Main.Prefix + "Verwende §e/setwarp <Zahl>");
            } else
                p.sendMessage(Main.noPerm);
        }
        return false;
    }
}
