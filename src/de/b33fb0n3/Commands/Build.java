package de.b33fb0n3.Commands;

import de.b33fb0n3.Lobby.Main;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;

/**
 * Plugin made by B33fb0n3YT
 * 12.07.2020
 * F*CKING SKIDDER!
 * Licensed by B33fb0n3YT
 * © All rights reserved
 */

public class Build implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;
            if (p.hasPermission("system.build")) {
                if (args.length == 0) {
                    Bukkit.getScheduler().runTask(Main.getPlugin(), new Runnable() {
                        @Override
                        public void run() {
                            if(!p.hasMetadata("builder")) {
                                p.setMetadata("builder", new FixedMetadataValue(Main.getPlugin(), true));
                            p.setGameMode(GameMode.CREATIVE);
                            p.sendMessage(Main.Prefix + "Du bist nun im Gamemode 1");
                            } else {
                                p.removeMetadata("builder", Main.getPlugin());
                                p.setGameMode(GameMode.ADVENTURE);
                                p.sendMessage(Main.Prefix + "Du bist nun im Gamemode 2");
                            }
                        }
                    });
                } else
                    p.sendMessage(Main.Prefix + "Verwende §e/build");
            } else
                p.sendMessage(Main.noPerm);
        }
        return false;
    }
}
