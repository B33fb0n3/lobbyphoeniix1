package de.b33fb0n3.Utils;

// Plugin made by B33fb0n3YT
// 12.02.2019
// F*CKING SKIDDER!

import de.b33fb0n3.Lobby.Main;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;


public class ConfigLocation {

    private Main plugin;
    private Location location;
    private String root;

    public ConfigLocation(Main plugin, Location location, String root) {
        this.plugin = plugin;
        this.location = location;
        this.root = root;
    }

    public ConfigLocation(Main plugin, String root) {
        this(plugin, null, root);
    }

    public void saveLocation() {
        FileConfiguration config = plugin.getConfig();
        config.set(root + ".World", location.getWorld().getName());
        config.set(root + ".X", location.getX());
        config.set(root + ".Y", location.getY());
        config.set(root + ".Z", location.getZ());
        config.set(root + ".Yaw", location.getYaw());
        config.set(root + ".Pitch", location.getPitch());
        plugin.saveConfig();
    }

    public Location loadLocation() {
        FileConfiguration config = plugin.getConfig();
        if (config.contains(root)) {
            World world = Bukkit.getWorld(config.getString(root + ".World"));
            double X = config.getDouble(root + ".X"),
                    Y = config.getDouble(root + ".Y"),
                    Z = config.getDouble(root + ".Z");
            float yaw = (float) config.getDouble(root + ".Yaw"),
                    pitch = (float) config.getDouble(root + ".Pitch");
            return new Location(world, X, Y, Z, yaw, pitch);
        } else
            return null;
    }
}
