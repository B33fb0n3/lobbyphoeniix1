package de.b33fb0n3.Utils;

import de.b33fb0n3.Lobby.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Team;

/**
 * Plugin made by B33fb0n3YT
 * 12.07.2020
 * F*CKING SKIDDER!
 * Licensed by B33fb0n3YT
 * © All rights reserved
 */

public class Scoreboard {
    public static void setScoreboard(Player p) {
        org.bukkit.scoreboard.Scoreboard board = p.getScoreboard();
        Objective obj = board.getObjective("lobby") != null ? board.getObjective("lobby") : board.registerNewObjective("lobby", "dummy");
        Team owner = board.getTeam("00000000OWNER");
        Team admin = board.getTeam("0000000ADMIN");
        Team mod = board.getTeam("000000MOD");
        Team dev = board.getTeam("00000DEV");
        Team sup = board.getTeam("0000SUP");
        Team builder = board.getTeam("000BUILDER");
        Team supplus = board.getTeam("00SUPPLUS");
        Team premium = board.getTeam("0PREMIUM");
        Team def = board.getTeam("DEFAULT");
        if (owner == null)
            owner = board.registerNewTeam("00000000OWNER");
        if (admin == null)
            admin = board.registerNewTeam("0000000ADMIN");
        if (mod == null)
            mod = board.registerNewTeam("000000MOD");
        if (dev == null)
            dev = board.registerNewTeam("00000DEV");
        if (sup == null)
            sup = board.registerNewTeam("0000SUP");
        if (builder == null)
            builder = board.registerNewTeam("000BUILDER");
        if (supplus == null)
            supplus = board.registerNewTeam("00SUPPLUS");
        if (premium == null)
            premium = board.registerNewTeam("0PREMIUM");
        if (def == null)
            def = board.registerNewTeam("DEFAULT");

        owner.setPrefix(ChatColor.translateAlternateColorCodes('&', "§4Owner §8| §f"));
        admin.setPrefix(ChatColor.translateAlternateColorCodes('&', "§4Admin §8| §f"));
        mod.setPrefix(ChatColor.translateAlternateColorCodes('&', "§cMod §8| §f"));
        dev.setPrefix(ChatColor.translateAlternateColorCodes('&', "§bDev §8| §f"));
        sup.setPrefix(ChatColor.translateAlternateColorCodes('&', "§9Sup §8| §f"));
        builder.setPrefix(ChatColor.translateAlternateColorCodes('&', "§eBuilder §8| §f"));
        supplus.setPrefix(ChatColor.translateAlternateColorCodes('&', "§5YT §8| §f"));
        premium.setPrefix(ChatColor.translateAlternateColorCodes('&', "§6Premium §8| §f"));
        def.setPrefix(ChatColor.translateAlternateColorCodes('&', "§7Spieler §8| §f"));

        obj.setDisplayName(Main.Prefix);
        obj.setDisplaySlot(DisplaySlot.SIDEBAR);

        for (Player all : Bukkit.getOnlinePlayers()) {
            if (all.hasPermission("system.owner")) {
                p.setDisplayName(owner.getPrefix() + p.getName());
                owner.addEntry(all.getName());
            } else if (all.hasPermission("system.admin")) {
                p.setDisplayName(admin.getPrefix() + p.getName());
                admin.addEntry(all.getName());
            } else if (all.hasPermission("system.dev")) {
                p.setDisplayName(dev.getPrefix() + p.getName());
                dev.addEntry(all.getName());
            } else if (all.hasPermission("lobby.builder")) {
                p.setDisplayName(builder.getPrefix() + p.getName());
                builder.addEntry(all.getName());
            } else if (all.hasPermission("lobby.mod")) {
                p.setDisplayName(mod.getPrefix() + p.getName());
                mod.addEntry(all.getName());
            } else if (all.hasPermission("lobby.supplus")) {
                p.setDisplayName(supplus.getPrefix() + p.getName());
                supplus.addEntry(all.getName());
            } else if (all.hasPermission("lobby.sup")) {
                p.setDisplayName(sup.getPrefix() + p.getName());
                sup.addEntry(all.getName());
            } else if (all.hasPermission("lobby.plus")) {
                p.setDisplayName(premium.getPrefix() + p.getName());
                premium.addEntry(all.getName());
            } else {
                p.setDisplayName(def.getPrefix() + p.getName());
                def.addEntry(all.getName());
            }
        }
        p.setScoreboard(board);
    }
}
